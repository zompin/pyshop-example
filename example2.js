'use strict'

function isEven(number) {
    // Returns True if **number** is even or False if it is odd.
    return !(number % 2);              
}

function equal(f, args, results) {
	let argsLen = args.length;
	let resLen = results.length;

	if (argsLen != resLen) {
		throw new Error('Lengths of args and resuts does not match');
	}

	for (let i = 0; i < argsLen; i++) {
		let res;

		if (Array.isArray(results)) {
			res = results[i] == f(args[i]);
		} else {
			res = results == f(arg[i]);
		}

		if (res) {
			console.log('Passed');
		} else {
			console.log('Failed');
		}
	}
}

equal(isEven, [1, 2, 3, 4], [false, true, false, true]);