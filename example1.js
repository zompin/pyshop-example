var matrixExample = [
	[ 1, 2, 3, 4 ],
	[ 4, 5, 6, 5 ],
	[ 7, 8, 9, 7 ],
	[ 7, 8, 9, 7 ]
];

function sumUpDiagonals(matrix) {
	let rowsNum = matrix.length;
	let colsNum;
	let summMainDiag = 0;
	let summSecondDiag = 0;

	matrix.forEach(function(item) {
		if (colsNum === undefined) {
			colsNum = item.length;
		} else {
			if (colsNum != item.length) {
				throw new Erros('Matrix is incorrect');
			}
		}
	});

	for (let i = 0, j = colsNum - 1; i < rowsNum; i++, j--) {
		summMainDiag += matrix[i][i];
		summSecondDiag += matrix[i][j];
	}

	return {
		summMainDiag: summMainDiag,
		summSecondDiag: summSecondDiag
	};
}

console.log(sumUpDiagonals(matrixExample))